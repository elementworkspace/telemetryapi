﻿using FluentValidation;
using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.AppConfig.Options;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Exceptions;
using TelemetryAPI.General.Exceptions.MediatR;
using TelemetryAPI.General.Services.DataStore;
using TelemetryAPI.General.Validation;
using static TelemetryAPI.Api.AccessLogs.Command.WriteAccessLogsAppInsights;

[assembly: FunctionsStartup(typeof(TelemetryAPI.AppConfig.Startup))]
namespace TelemetryAPI.AppConfig
{
    public class Startup : FunctionsStartup
    {
        private IConfiguration _config;

        public IConfiguration Configuration
        {
            get
            {
                return _config;
            }
        }

        public Startup()
        { }

        public Startup(IConfiguration configuration)
        {
            _config = configuration;
        }

        public override void Configure(IFunctionsHostBuilder builder)
        {
            

            var env = GetEnvironmentVariable("ENVIRONMENT");
            if (string.IsNullOrEmpty(env))
            {
                env = "development";
            }

            var services = builder.Services;

            // Configure services
            // Configure dependency injection
            var assembly = Assembly.GetExecutingAssembly();
            services.AddMediatR(assembly);
            services.AddLogging();
            //services.AddFluentValidation();
            services.AddValidatorsFromAssembly(assembly);

            services.AddOptions<AzureDataLakeOptions>()
                .Configure<IConfiguration>((settings, configuration) => { configuration.Bind("AzureDataLake", settings); });

            this.ConfigureServices(builder.Services);

           
        }

        public override void ConfigureAppConfiguration(IFunctionsConfigurationBuilder builder)
        {
            this.Configure(builder);
        }

        private static string GetEnvironmentVariable(string name)
        {
            return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDataStoreService, AzureDataLakeService>();
            services.AddTransient(typeof(IRequestExceptionHandler<,,>), typeof(MediatRGenericExceptionHandler<,,>));
            services.AddScoped(typeof(IRequestExceptionHandler<Command, TelemetryOperationStatus, Exception>), typeof(AppInsightsAccessLogCommandExceptionHandler));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient<ExceptionHandlingMiddleware>();
        }

        public virtual void Configure(IFunctionsConfigurationBuilder configurationBuilder)
        {
            // Configure application
            FunctionsHostBuilderContext context = configurationBuilder.GetContext();

            _config = configurationBuilder.ConfigurationBuilder
                .AddJsonFile(Path.Combine(context.ApplicationRootPath, "AppConfig\\appsettings.json"), optional: true, reloadOnChange: false)
                .AddJsonFile(Path.Combine(context.ApplicationRootPath, $"AppConfig\\appsettings.{context.EnvironmentName}.json"), optional: true, reloadOnChange: false)
                .AddEnvironmentVariables().Build();
        }
    }
}
