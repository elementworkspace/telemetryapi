﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.General.Enum;

namespace TelemetryAPI.General.Entity
{
    public class MetricBase : LogItemBase
    {
        public String Title { get; set; }
        public String Body { get; set; }
        public String Additional { get; set; }
        public String Url { get; set; }
        public int StatusCode { get; set; }
        public Double PayloadSize { get; set; }
        public long ReceivedResponseAtMillis { get; set; }
        public long SentRequestAtMillis { get; set; }
        public MetricsLogType LogType { get; set; }
        public double ElapsedTimeInSeconds { get; set; }
        public double ElapsedTimeInMS { get; set; }
        public String Category { get; set; }
    }
}
