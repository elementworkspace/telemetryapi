﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Entity
{
    public class AccessLogBase
    {
        public String ClientIP { get; set; }
        public String UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public String Method { get; set; }
        public String RequestURL { get; set; }
        public String Protocol { get; set; }
        public int StatusCode { get; set; }
        public int PayloadSize { get; set; }
        public String BorwserAgent { get; set; }
        public String RequestId { get; set; }
    }
}
