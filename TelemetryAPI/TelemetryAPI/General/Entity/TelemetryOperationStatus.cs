﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Entity
{
    public class TelemetryOperationStatus
    {
        [JsonConstructor]
        public TelemetryOperationStatus(bool isSuccess, int telemetryItemsCount = 0, string message = null)
        {
            IsSuccess = isSuccess;
            TelemetryItemsCount = telemetryItemsCount;
            Message = message;
        }

        public bool IsSuccess { get; private set; } = false;
        public int TelemetryItemsCount { get; private set; } = 0;

        public string Message { get; private set; } = string.Empty;
    }
}
