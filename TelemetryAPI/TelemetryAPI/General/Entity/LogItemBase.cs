﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Entity
{
    public class LogItemBase
    {
        public String RequestId { get; set; }
        public String CorrelationId { get; set; }
        public String UserId { get; set; }
        public String EnvironmentId { get; set; }
        public String AppName { get; set; }
        public String AppVersion { get; set; }
        public DateTime CreatedAt { get; set; }

        public String Message { get; set; }
    }
}
