﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.General.Enum;

namespace TelemetryAPI.General.Entity
{
    public class MessageQueueLogBase : LogItemBase
    {
        public String SourceHostname { get; set; }
        public String SourceAppName { get; set; }
        public String SourceAppVersion { get; set; }
        public String SourceEnvironmentId { get; set; }
        public String SourceRequestId { get; set; }
        public String SourceUserId { get; set; }
        public MessageType MessageType { get; set; }
    }
}
