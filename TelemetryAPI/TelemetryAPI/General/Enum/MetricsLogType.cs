﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Enum
{
    public enum MetricsLogType
    {
        Api,
        Database,
        Code
    }
}
