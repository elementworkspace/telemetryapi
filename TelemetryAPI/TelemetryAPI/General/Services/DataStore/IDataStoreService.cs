﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Services.DataStore
{
    public interface IDataStoreService
    {
        void SetRootDirectory(string dataRootDirectoryPath);

        Task<DataOperationStatus> WriteDataDump(string data, DateTime date, string filePreFix);
        Task<DataOperationStatus> WriteDataDump(string data, int year, int month, int day, string filePreFix);

    }
}
