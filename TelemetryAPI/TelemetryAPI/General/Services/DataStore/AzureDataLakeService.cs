﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Files.DataLake;
using Azure.Storage.Files.DataLake.Models;
using Azure.Storage;
using Azure.Identity;
using System.IO;
using Microsoft.Extensions.Configuration;
using TelemetryAPI.AppConfig.Options;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace TelemetryAPI.General.Services.DataStore
{
    internal class AzureDataLakeService : IDataStoreService
    {
        private DataLakeServiceClient _serviceClient;
        private DataLakeFileSystemClient _fileSystemClient;
        private readonly ILogger<AzureDataLakeService> _logger;
        private string _rootDirectoryPath;

        public AzureDataLakeService(IOptions<AzureDataLakeOptions> configuration, ILogger<AzureDataLakeService> logger)
        {
            VerifyDataStoreInitState(configuration);
            _logger = logger;
        }

        private async void VerifyDataStoreInitState(IOptions<AzureDataLakeOptions> configuration)
        {
            var environemntType = Environment.GetEnvironmentVariable("ENVIRONMENT", EnvironmentVariableTarget.Process);
            var azureStorageUrl = configuration.Value.AzureStorageUrl;
            if (string.IsNullOrEmpty(azureStorageUrl))
                throw new Exception("AzureStorageUrl configuration is not defined for the Data Storage Telemetry Data Dump.");

            if (environemntType == "development")
            {
                StorageSharedKeyCredential sharedKeyCredential = new StorageSharedKeyCredential(configuration.Value.AccountName, configuration.Value.AccountKey);
                _serviceClient = new DataLakeServiceClient(new Uri(azureStorageUrl), sharedKeyCredential);
            }
            else
            {
                _serviceClient = new DataLakeServiceClient(new Uri(azureStorageUrl), new DefaultAzureCredential());
            }
        }


        public void SetRootDirectory(string dataRootDirectoryPath)
        {
            if (string.IsNullOrEmpty(dataRootDirectoryPath))
                throw new Exception("TelemetryDataStoreRootDirectory configuration is not defined for the Data Storage Telemetry Data Dump.");

            _rootDirectoryPath = dataRootDirectoryPath;
            var directoriesToDataRoot = dataRootDirectoryPath.Split('/');
            if (directoriesToDataRoot.Length > 0)
            {
                _fileSystemClient = _serviceClient.GetFileSystemClient(directoriesToDataRoot[0]);
                _fileSystemClient.CreateIfNotExists();
            }
        }

        private bool VerifyDataLakeConnection()
        {
            if (_fileSystemClient != null && _serviceClient != null)
                return true;

            _logger.LogError("Data Lake configurations are not setup correctly: Missing root directry value or the client connection is not established correctly");

            return false;
        }

        public async Task<DataOperationStatus> WriteDataDump(string data, DateTime date, string filePreFix)
        {
            return await WriteDataDump(data, date.Year, date.Month, date.Day, filePreFix);
        }

        public async Task<DataOperationStatus> WriteDataDump(string data, int year, int month, int day, string filePreFix)
        {
            if (!VerifyDataLakeConnection())
                return new DataOperationStatus(false, "Data Lake configurations are not setup correctly.");

            long bytesWritten = 0;
            try
            {
                var file = _fileSystemClient.GetFileClient($"{_rootDirectoryPath}/{year}/{month}/{day}/{filePreFix}-{year}-{month}-{day}-{DateTime.UtcNow.Ticks}.json");
                var dataStream = BinaryData.FromString(data).ToStream();
                await file.UploadAsync(dataStream, false);
                bytesWritten = dataStream.Length;
            }
            catch (RequestFailedException reqFailedEx)
            {
                _logger.LogWarning(reqFailedEx, "Failed to write given data to Azure Data Lake.");
                _logger.LogDebug(data);
                return new DataOperationStatus(false, "Failed to write given data to Azure Data Lake.");
            }

            return new DataOperationStatus(true, string.Empty, bytesWritten);
        }
    }
}
