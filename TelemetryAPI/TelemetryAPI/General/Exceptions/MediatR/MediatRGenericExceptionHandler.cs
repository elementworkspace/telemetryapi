﻿using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TelemetryAPI.General.Exceptions.MediatR
{
    public class MediatRGenericExceptionHandler<TRequest, TResponse, TException> : IRequestExceptionHandler<TRequest, TResponse, TException>
    where TException : Exception where TRequest : IRequest<TResponse>
    {
        private readonly ILogger _logger;
        public MediatRGenericExceptionHandler(ILogger<MediatRGenericExceptionHandler<TRequest, TResponse, TException>> logger)
        {
            _logger = logger;
        }
        public async Task Handle(TRequest request,
            TException exception,
            RequestExceptionHandlerState<TResponse> state,
            CancellationToken cancellationToken)
        {
            _logger.LogError(exception, "General error in MediatR pipeline, unhandled error.");
            // TODO: Any unhandled commands related to the telemetry operation should be sent to a event queue for retry at a later stage
        }
    }
}
