﻿using MediatR.Pipeline;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using static TelemetryAPI.Api.AccessLogs.Command.WriteAccessLogsAppInsights;

namespace TelemetryAPI.General.Exceptions.MediatR
{
    public class AppInsightsAccessLogCommandExceptionHandler : RequestExceptionHandler<Command, TelemetryOperationStatus, Exception>
    {
        private readonly ILogger _logger;
        public AppInsightsAccessLogCommandExceptionHandler(ILogger<AppInsightsAccessLogCommandExceptionHandler> logger)
        {
            _logger = logger;
        }

        protected override async void Handle(Command request, Exception exception, RequestExceptionHandlerState<TelemetryOperationStatus> state)
        {
            _logger.LogError(exception, "AppInsightsAccessLogCommandExceptionHandler error in MediatR pipeline.");
            // TODO: Any unhandled commands related to the telemetry operation should be sent to a event queue for retry at a later stage
        }
    }
}
