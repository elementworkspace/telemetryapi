﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.Api.Logging;
using DataStoreType = TelemetryAPI.Api.AccessLogs.Command.WriteAccessLogsDataStore.AccessLog;
using AppInsightsType = TelemetryAPI.Api.AccessLogs.Command.WriteAccessLogsAppInsights.AccessLog;

namespace TelemetryAPI.General.Extensions
{
    public static class AccessLogsExtension
    {
        public static IList<AppInsightsType> ToAppInsights(this IList<TelemetryAPI.General.Entity.AccessLogBase> logItems)
        {
            return new List<AppInsightsType>();
        }
        public static IList<DataStoreType> ToDataStore(this IList<TelemetryAPI.General.Entity.AccessLogBase> logItems)
        {
            return logItems.Select(logItem => new DataStoreType()
            {
                BorwserAgent = logItem.BorwserAgent,
                StatusCode = logItem.StatusCode,
                ClientIP = logItem.ClientIP,
                Method = logItem.Method,
                PayloadSize = logItem.PayloadSize,
                Protocol = logItem.Protocol,
                RequestId = logItem.RequestId,
                RequestURL = logItem.RequestURL,
                Timestamp = logItem.Timestamp,
                UserId = logItem.UserId,
            }).ToList();
        }
    }
}
