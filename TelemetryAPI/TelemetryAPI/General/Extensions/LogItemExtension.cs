﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.Api.Logging;
using LogItemDataStore = TelemetryAPI.Api.Logging.Command.WriteLogItemsDataStore.LogItem;

namespace TelemetryAPI.General.Extensions
{
    public static class LogItemExtension
    {
        public static IList<LogItemDataStore> ToDataStore(this IList<TelemetryAPI.General.Entity.LogItemBase> logItems)
        {
            return logItems.Select(logItem => new LogItemDataStore()
            {
                AppName = logItem.AppName,
                AppVersion = logItem.AppVersion,
                CorrelationId = logItem.CorrelationId,
                CreatedAt = logItem.CreatedAt,
                EnvironmentId = logItem.EnvironmentId,
                RequestId = logItem.RequestId,
                UserId = logItem.UserId,
                Message = logItem.Message,
            }).ToList();
        }
    }
}
