﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.Api.Logging;
using DataStoreType = TelemetryAPI.Api.Metrics.Command.WriteMetricsDataStore.Metric;

namespace TelemetryAPI.General.Extensions
{
    public static class MetricExtension
    {
        public static IList<DataStoreType> ToDataStore(this IList<TelemetryAPI.General.Entity.MetricBase> logItems)
        {
            return logItems.Select(logItem => new DataStoreType()
            {
                AppName = logItem.AppName,
                AppVersion = logItem.AppVersion,
                CorrelationId = logItem.CorrelationId,
                CreatedAt = logItem.CreatedAt,
                EnvironmentId = logItem.EnvironmentId,
                RequestId = logItem.RequestId,
                UserId = logItem.UserId,
                Message = logItem.Message,
                Additional = logItem.Additional,
                Body = logItem.Body,
                Category = logItem.Category,
                ElapsedTimeInMS = logItem.ElapsedTimeInMS,
                ElapsedTimeInSeconds = logItem.ElapsedTimeInSeconds,
                LogType = logItem.LogType,
                PayloadSize = logItem.PayloadSize,
                ReceivedResponseAtMillis = logItem.ReceivedResponseAtMillis,
                SentRequestAtMillis = logItem.SentRequestAtMillis,
                StatusCode = logItem.StatusCode,
                Title = logItem.Title,
                Url = logItem.Url
            }).ToList();
        }
    }
}
