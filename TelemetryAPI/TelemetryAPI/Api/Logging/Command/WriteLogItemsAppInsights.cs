﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Extensions;

namespace TelemetryAPI.Api.Logging.Command
{
    public class WriteLogItemsAppInsights
    {
        public class LogItem : LogItemBase
        {

        }

        public class AppInsightsLogCommand : IRequest<TelemetryOperationStatus>
        {
            public IList<LogItemBase> LoggItems { get; set; }
        }

        public class AppInsightsLogCommandHandler : IRequestHandler<AppInsightsLogCommand, TelemetryOperationStatus>
        {
            public async Task<TelemetryOperationStatus> Handle(AppInsightsLogCommand request, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }
        }
    }
}
