using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using TelemetryAPI.General.Entity;
using TelemetryAPI.Api.Metrics.Command;

namespace TelemetryAPI.API.Metrics
{
    public class AccessLogsController
    {
        private readonly ILogger<AccessLogsController> _logger;
        private readonly IMediator _mediator;

        public AccessLogsController(ILogger<AccessLogsController> log, IMediator mediator)
        {
            _logger = log;
            _mediator = mediator;
        }

        [FunctionName("Metrics")]
        [OpenApiOperation(operationId: "Metrics", tags: new[] { "name" })]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiRequestBody(contentType: "application/json", bodyType: typeof(IList<TelemetryAPI.General.Entity.MetricBase>), Description = "A List of logg items to be saved.")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "application/json", bodyType: typeof(TelemetryOperationStatus), Description = "The OK response")]
        public async Task<IActionResult> LogsRun(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req)
        {
            _logger.LogInformation("C# HTTP trigger function request for LogItems.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            
            _logger.LogDebug(requestBody);

            var data = JsonConvert.DeserializeObject<IList<TelemetryAPI.General.Entity.MetricBase>>(requestBody);

            IList<Task> telemetryOperatorCommands = new List<Task>();
            telemetryOperatorCommands.Add(this._mediator.Send(new WriteMetricsDataStore.Command() { MetricItems = data }));

            await Task.WhenAll(telemetryOperatorCommands);

            return new OkObjectResult(new TelemetryOperationStatus(telemetryOperatorCommands.All(o => o.IsCompletedSuccessfully), data.Count));
        }
    }
}

