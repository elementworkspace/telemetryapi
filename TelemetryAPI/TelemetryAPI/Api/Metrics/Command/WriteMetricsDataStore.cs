﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Services.DataStore;
using TelemetryAPI.General.Extensions;
using FluentValidation;

namespace TelemetryAPI.Api.Metrics.Command
{
    public class WriteMetricsDataStore
    {
        public class Metric : MetricBase
        {
        }
        public class Command : IRequest<TelemetryOperationStatus>
        {
            public IList<MetricBase> MetricItems { get; set; }
        }

        public sealed class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(x => x.MetricItems).NotEmpty();
                RuleFor(x => x.MetricItems).NotNull();
            }
        }

        public class CommandHandler : IRequestHandler<Command, TelemetryOperationStatus>
        {
            private readonly IDataStoreService _dataStoreService;
            public CommandHandler(IDataStoreService dataStoreService)
            {
                _dataStoreService = dataStoreService;
                _dataStoreService.SetRootDirectory("telemertyapi/raw/metrics");
            }

            public async Task<TelemetryOperationStatus> Handle(Command request, CancellationToken cancellationToken)
            {
                if (request.MetricItems == null)
                    return new TelemetryOperationStatus(false);

                int itemsProcessed = 0;

                var logItemsGroupedByYear = request.MetricItems.GroupBy(o => o.CreatedAt.Year);
                foreach (var logsItemYear in logItemsGroupedByYear)
                {
                    var logItemsGroupedByMonth = logsItemYear.GroupBy(o => o.CreatedAt.Month);
                    foreach (var logsItemMonth in logItemsGroupedByMonth)
                    {
                        var logItemsGroupedByDay = logsItemMonth.GroupBy(o => o.CreatedAt.Day);
                        foreach (var logsItemDay in logItemsGroupedByDay)
                        {
                            var items = logsItemDay.ToList();
                            var dataOperationStatus = await _dataStoreService.WriteDataDump(JsonSerializer.Serialize(items.ToDataStore()), logsItemYear.Key, logsItemMonth.Key, logsItemDay.Key, "metrics");
                            if (dataOperationStatus != null && dataOperationStatus.IsSuccess)
                            {
                                itemsProcessed += items.Count;
                            }
                        }
                    }
                }

                return new TelemetryOperationStatus(itemsProcessed == request.MetricItems.Count ? true : false, itemsProcessed);
            }
        }
    }
}
