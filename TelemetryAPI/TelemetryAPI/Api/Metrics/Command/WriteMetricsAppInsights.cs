﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Extensions;

namespace TelemetryAPI.Api.Metrics.Command
{
    public class WriteMetricsAppInsights
    {
        public class Metric : MetricBase
        {
        }
        public class Command : IRequest<TelemetryOperationStatus>
        {
            public IList<MetricBase> MetricItems { get; set; }
        }

        public class CommandHandler : IRequestHandler<Command, TelemetryOperationStatus>
        {
            public async Task<TelemetryOperationStatus> Handle(Command request, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }
        }
    }
}
