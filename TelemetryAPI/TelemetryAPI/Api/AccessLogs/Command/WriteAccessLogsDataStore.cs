﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Services.DataStore;
using TelemetryAPI.General.Extensions;
using FluentValidation;

namespace TelemetryAPI.Api.AccessLogs.Command
{
    public class WriteAccessLogsDataStore
    {
        public class AccessLog : AccessLogBase
        {
        }

        public class Command : IRequest<TelemetryOperationStatus>
        {
            public IList<AccessLogBase> AccessLogItems { get; set; }
        }

        public sealed class Validator : AbstractValidator<Command>
        {
            public Validator()
            {
                RuleFor(x => x.AccessLogItems).NotEmpty();
                RuleFor(x => x.AccessLogItems).NotNull();
            }
        }

        public class CommandHandler : IRequestHandler<Command, TelemetryOperationStatus>
        {
            private readonly IDataStoreService _dataStoreService;
            public CommandHandler(IDataStoreService dataStoreService)
            {
                _dataStoreService = dataStoreService;
                _dataStoreService.SetRootDirectory("telemertyapi/raw/accesslogs");
            }

            public async Task<TelemetryOperationStatus> Handle(Command request, CancellationToken cancellationToken)
            {
                if (request.AccessLogItems == null)
                    return new TelemetryOperationStatus(false);

                int itemsProcessed = 0;

                var logItemsGroupedByYear = request.AccessLogItems.GroupBy(o => o.Timestamp.Year);
                foreach (var logsItemYear in logItemsGroupedByYear)
                {
                    var logItemsGroupedByMonth = logsItemYear.GroupBy(o => o.Timestamp.Month);
                    foreach (var logsItemMonth in logItemsGroupedByMonth)
                    {
                        var logItemsGroupedByDay = logsItemMonth.GroupBy(o => o.Timestamp.Day);
                        foreach (var logsItemDay in logItemsGroupedByDay)
                        {
                            var items = logsItemDay.ToList();
                            var dataOperationStatus = await _dataStoreService.WriteDataDump(JsonSerializer.Serialize(items.ToDataStore()), logsItemYear.Key, logsItemMonth.Key, logsItemDay.Key, "accesslogs");
                            if (dataOperationStatus != null && dataOperationStatus.IsSuccess)
                            {
                                itemsProcessed += items.Count;
                            }
                        }
                    }
                }

                return new TelemetryOperationStatus(itemsProcessed == request.AccessLogItems.Count ? true : false, itemsProcessed);
            }
        }
    }
}
