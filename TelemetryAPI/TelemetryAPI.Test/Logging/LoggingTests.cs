﻿using Bogus;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Services.DataStore;
using Xunit;
using static TelemetryAPI.Api.Logging.Command.WriteLogItemsDataStore;
using FluentValidation.TestHelper;

namespace TelemetryAPI.UnitTests.Logging
{
    public class LoggingTests
    {

        private List<LogItemBase> CreateDataItemsForLogging()
        {
            var faker = new Faker("en");
            var dataItems = new List<LogItemBase>();
            dataItems.Add(new LogItemBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow,
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
            });
            dataItems.Add(new LogItemBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddDays(-5),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
            });
            dataItems.Add(new LogItemBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddDays(-60),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
            });
            dataItems.Add(new LogItemBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddYears(-1),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
            });

            return dataItems;
        }
        [Fact]
        public async void DataStoreLoggingTestForMultipleLogItems()
        {

            var dataItems = CreateDataItemsForLogging();
            var azureDataLakeServiceMock = new Mock<IDataStoreService>();
            azureDataLakeServiceMock.Setup(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>())).ReturnsAsync(new DataOperationStatus(true));

            var command = new Command() { LogItems = dataItems };
            var handler = new CommandHandler(azureDataLakeServiceMock.Object);

            var telemetryOperationStatus = await handler.Handle(command, CancellationToken.None);

            telemetryOperationStatus.IsSuccess.ShouldBe(true);
            telemetryOperationStatus.TelemetryItemsCount.ShouldBe(dataItems.Count);

            azureDataLakeServiceMock.Verify(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>()), Times.AtLeast(dataItems.DistinctBy(o => o.CreatedAt).Count()));
        }

        [Fact]
        public async void DataStoreAccessLogTest_EmptyDataValidation()
        {
            Validator validator = new Validator();
            var result = await validator.TestValidateAsync(new Command() { LogItems = null });
            result.IsValid.ShouldBeFalse();
        }
    }
}
