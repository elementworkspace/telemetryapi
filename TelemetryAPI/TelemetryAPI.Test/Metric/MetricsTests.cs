﻿using Bogus;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Services.DataStore;
using Xunit;
using System.Threading;
using Shouldly;
using TelemetryAPI.Api.Metrics.Command;
using FluentValidation.TestHelper;

namespace TelemetryAPI.UnitTests.Metric
{
    public class MetricsTests
    {
        private new List<MetricBase> CreateMetricsDataItems()
        {
            var faker = new Faker("en");
            var dataItems = new List<MetricBase>();
            dataItems.Add(new MetricBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow,
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
                Additional = faker.Random.String(25),
                Body = faker.Random.String(2048),
                Category = faker.Random.String(128),
                ElapsedTimeInMS = faker.Random.Double(),
                ElapsedTimeInSeconds = faker.Random.Double(),
                LogType = General.Enum.MetricsLogType.Code,
                PayloadSize = faker.Random.Double(),
                ReceivedResponseAtMillis = faker.Random.Long(),
                SentRequestAtMillis = faker.Random.Long(),
                StatusCode = faker.Random.Int(),
                Title = faker.Random.String(256),
                Url = faker.Random.String(512)
            });
            dataItems.Add(new MetricBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddDays(-5),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
                Additional = faker.Random.String(25),
                Body = faker.Random.String(2048),
                Category = faker.Random.String(128),
                ElapsedTimeInMS = faker.Random.Double(),
                ElapsedTimeInSeconds = faker.Random.Double(),
                LogType = General.Enum.MetricsLogType.Code,
                PayloadSize = faker.Random.Double(),
                ReceivedResponseAtMillis = faker.Random.Long(),
                SentRequestAtMillis = faker.Random.Long(),
                StatusCode = faker.Random.Int(),
                Title = faker.Random.String(256),
                Url = faker.Random.String(512)
            });
            dataItems.Add(new MetricBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddDays(-60),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
                Additional = faker.Random.String(25),
                Body = faker.Random.String(2048),
                Category = faker.Random.String(128),
                ElapsedTimeInMS = faker.Random.Double(),
                ElapsedTimeInSeconds = faker.Random.Double(),
                LogType = General.Enum.MetricsLogType.Api,
                PayloadSize = faker.Random.Double(),
                ReceivedResponseAtMillis = faker.Random.Long(),
                SentRequestAtMillis = faker.Random.Long(),
                StatusCode = faker.Random.Int(),
                Title = faker.Random.String(256),
                Url = faker.Random.String(512)
            });
            dataItems.Add(new MetricBase()
            {
                AppName = faker.Random.String(25),
                AppVersion = faker.Random.String(5),
                CorrelationId = faker.Random.Guid().ToString(),
                CreatedAt = DateTime.UtcNow.AddYears(-1),
                EnvironmentId = faker.Random.Guid().ToString(),
                Message = faker.Random.String(1024),
                RequestId = faker.Random.Guid().ToString(),
                UserId = faker.Random.Guid().ToString(),
                Additional = faker.Random.String(25),
                Body = faker.Random.String(2048),
                Category = faker.Random.String(128),
                ElapsedTimeInMS = faker.Random.Double(),
                ElapsedTimeInSeconds = faker.Random.Double(),
                LogType = General.Enum.MetricsLogType.Database,
                PayloadSize = faker.Random.Double(),
                ReceivedResponseAtMillis = faker.Random.Long(),
                SentRequestAtMillis = faker.Random.Long(),
                StatusCode = faker.Random.Int(),
                Title = faker.Random.String(256),
                Url = faker.Random.String(512)
            });

            return dataItems;
        }
        [Fact]
        public async void DataStoreLoggingTestForMultipleLogItems()
        {

            var dataItems = CreateMetricsDataItems();
            var azureDataLakeServiceMock = new Mock<IDataStoreService>();
            azureDataLakeServiceMock.Setup(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>())).ReturnsAsync(new DataOperationStatus(true));

            var command = new WriteMetricsDataStore.Command() { MetricItems = dataItems };
            var handler = new WriteMetricsDataStore.CommandHandler(azureDataLakeServiceMock.Object);

            var telemetryOperationStatus = await handler.Handle(command, CancellationToken.None);

            telemetryOperationStatus.IsSuccess.ShouldBe(true);
            telemetryOperationStatus.TelemetryItemsCount.ShouldBe(dataItems.Count);

            azureDataLakeServiceMock.Verify(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>()), Times.AtLeast(dataItems.DistinctBy(o => o.CreatedAt).Count()));
        }

        [Fact]
        public async void DataStoreAccessLogTest_EmptyDataValidation()
        {
            WriteMetricsDataStore.Validator validator = new WriteMetricsDataStore.Validator();
            var result = await validator.TestValidateAsync(new WriteMetricsDataStore.Command() { MetricItems = null });
            result.IsValid.ShouldBeFalse();
        }
    }
}
