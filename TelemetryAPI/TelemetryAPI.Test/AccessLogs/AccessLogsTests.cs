﻿using Bogus;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TelemetryAPI.General.Entity;
using TelemetryAPI.General.Services.DataStore;
using Xunit;
using static TelemetryAPI.Api.AccessLogs.Command.WriteAccessLogsDataStore;
using FluentValidation.TestHelper;

namespace TelemetryAPI.UnitTests.AccessLogs
{
    public class AccessLogsTests
    {

        private List<AccessLogBase> CreateAccessLogDataItems()
        {
            var faker = new Faker("en");
            var dataItems = new List<AccessLogBase>();
            dataItems.Add(new AccessLogBase()
            {
                BorwserAgent = faker.Internet.UserAgent(),
                ClientIP = faker.Internet.Ip(),
                Method = faker.Internet.Random.String(3),
                PayloadSize = faker.Random.Int(),
                Protocol = faker.Internet.Protocol(),
                RequestId = faker.Random.Guid().ToString(),
                RequestURL = faker.Internet.Url(),
                StatusCode = faker.Random.Int(),
                Timestamp = DateTime.UtcNow,
                UserId = faker.Random.Guid().ToString()
            });

            dataItems.Add(new AccessLogBase()
            {
                BorwserAgent = faker.Internet.UserAgent(),
                ClientIP = faker.Internet.Ip(),
                Method = faker.Internet.Random.String(3),
                PayloadSize = faker.Random.Int(),
                Protocol = faker.Internet.Protocol(),
                RequestId = faker.Random.Guid().ToString(),
                RequestURL = faker.Internet.Url(),
                StatusCode = faker.Random.Int(),
                Timestamp = DateTime.UtcNow.Subtract(new TimeSpan(1, 0, 0, 0)),
                UserId = faker.Random.Guid().ToString()
            });

            dataItems.Add(new AccessLogBase()
            {
                BorwserAgent = faker.Internet.UserAgent(),
                ClientIP = faker.Internet.Ip(),
                Method = faker.Internet.Random.String(3),
                PayloadSize = faker.Random.Int(),
                Protocol = faker.Internet.Protocol(),
                RequestId = faker.Random.Guid().ToString(),
                RequestURL = faker.Internet.Url(),
                StatusCode = faker.Random.Int(),
                Timestamp = DateTime.UtcNow.AddYears(-1),
                UserId = faker.Random.Guid().ToString()
            });

            return dataItems;
        }
        [Fact]
        public async void DataStoreAccessLogTest_For_MultipleAccessLogItems()
        {

            var dataItems = CreateAccessLogDataItems();
            var azureDataLakeServiceMock = new Mock<IDataStoreService>();
            azureDataLakeServiceMock.Setup(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>())).ReturnsAsync(new DataOperationStatus(true));
            var command = new Command() { AccessLogItems = dataItems };
            var handler = new CommandHandler(azureDataLakeServiceMock.Object);

            var telemetryOperationStatus = await handler.Handle(command, CancellationToken.None);

            telemetryOperationStatus.IsSuccess.ShouldBe(true);
            telemetryOperationStatus.TelemetryItemsCount.ShouldBe(dataItems.Count);

            azureDataLakeServiceMock.Verify(p => p.WriteDataDump(It.IsAny<String>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<String>()), Times.AtLeast(dataItems.DistinctBy( o => o.Timestamp).Count()));

        }

        [Fact]
        public async void DataStoreAccessLogTest_EmptyDataValidation()
        {
            Validator validator = new Validator();
            var result = await validator.TestValidateAsync(new Command() { AccessLogItems = null });
            result.IsValid.ShouldBeFalse();
        }
    }
}
