# Telemetry API

The Telemetry API is an API intended to be used as a common gateway for all logging operations in a distributed systems architecture. The work is in progress.

Present features:

- Storing data into Azure Data Lake for backup and history
- Support for metrics, access logs and general logs

## TODO

- Refacotring the Azure Function into an isolated version
- Error handling if a certain operation fails it should be stored into a queue/event store for later processing so that the data is not lost
- Integration tests
